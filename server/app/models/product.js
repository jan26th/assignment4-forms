var mongoose = require('mongoose'),
    schema = mongoose.Schema;

var priceSchema = schema({

    hasPrfCharge: {type: Boolean,required:true},
    enablenlpinterval: {type: Number, required:true},
    hasEcoFee: {type: Boolean, required:true},
    isUnitPricePrimary: {type: Boolean, required:true},
    usesUnitPriceMeasure: {type: Boolean, required:true},
    hasTemporaryFamilyOffer: {type: Boolean, required:true},
    hasFamilyPrice: {type: Boolean, required:true},
    comparisonPriceExists: {type: Boolean, required:true},

    normal: {
        priceNormalPerUnit: {
            unit: {type: String}
        },

        priceNormal: {
            rawPrice: {type: Number},
            value: {type: String},
            priceExclVat: {type: String},
            perUnit: {type: String}
        },

        priceNormalDual: {type: Object}
    }
});

var imageSchema=schema({

    large: [String],
    thumb: [String],
    small: [String],
    normal: [String],
    zoom: [String]

});

var itemSchema= schema({

    californiaTitle20Product: {type:Boolean,required:true},
    prices: priceSchema,
    buyable: {type:Boolean,required:true},
    metric: {type:String,required:true},
    color: {type:String,required:true},
    custBenefit: {type:String,required:true},
    environment: {type:String,required:true},
    availabilityUrl: {type:String,required:true},
    packagePopupUrl: {type:String,required:true},
    url: {type:String,required:true},
    goodToKnow: {type:String,required:true},
    nopackages: {type:String,required:true},
    designer: {type:String,required:true},
    techInfoArr: [],
    careInst: {type:String,required:true},
    validDesign: [String],
    goodToKnowPIP: {type:String,required:true},
    partNumber: {type:String,required:true},
    attachments: [],
    bit: {type:Boolean,required:true},
    name: {type:String,required:true},
    soldSeparately: {type:String,required:true},
    reqAssembly: {type:Boolean,required:true},
    metricPackageInfo: [metricPackageInfoSchema],
    type: {type:String,required:true},
    dualCurrencies: {type:Boolean,required:true},
    catEntryId: {type:String,required:true},
    descriptiveAttributes: {type:Object,required:true},
    imperial: {type:String,required:true},
    custMaterials: {type:String,required:true}

});

var metricPackageInfoSchema=schema({

    quantity: {type:String,required:true},
    length: {type:Number,required:true},
    width: {type:Number,required:true},
    articleNumber: {type:String,required:true},
    wieght: {type:Number,required:true},
    height: {type:Number,required:true}

});

var attachmentsSchema=schema({
    //TODO
});

var validDesignSchema=schema({
    //TODO
});

var attributesSchema=schema ({

    type: {type:String,required:true},
    name: {type:String,required:true},
    id: {type:String,required:true}

});

var productSchema=schema({

    partNumber: {type:String, required:true},
    attributes: [attributesSchema],
    catEntryId: {type:String, required:true},
    items: [itemSchema]

});

